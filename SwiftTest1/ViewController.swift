//
//  ViewController.swift
//  SwiftTest1
//
//  Created by Jason Clinger on 2/20/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    var d:AnyObject?
    var json:Array<AnyObject>!
    var data:NSData!
    let array = ["item1", "item2", "item3"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // the ! means that this is not optional, this NSURL object has to exist or this just won't work, it can't
        // be optional
        data = NSData(contentsOfURL: NSURL(string: "http://aasquaredapps.com/Class/sevenwonders.json")!)
        
        // needs to return optionally as Array, "as?" means that it needs to return, optionally followed by the type
        // it can optionally be returned by
        // Also the data! means that what data refers to MUST exist.
        do {
            json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as? Array
        } catch {
            print(error)
        }
    
        //d! in order for this to work, d has to exist, we have to have a dictionary d.
        //in order for NSLog to print this Sting out, this whole thing must exist
        
        //NSLog("%@", (d!["region"] as? String)!)
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationController?.navigationBar.alpha = 0.5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return array.count
        //return d!.count
        return 7
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCellWithIdentifier("customcell")! as UITableViewCell
        let cell = tableView.dequeueReusableCellWithIdentifier("customcell") as! customcell
        
        cell.textLabel?.textColor = UIColor.blueColor()
        d = json[indexPath.row] as? [String:AnyObject]
        cell.textLabel?.text = d!["name"] as? String
        cell.Wonder = d!["name"] as? String
        cell.yearBuilt = d!["year_built"] as? String
        cell.wonderLocation = d!["location"] as? String
        cell.wonderRegion = d!["region"] as? String
        
        //cell.wonderImage = d!["image_thumb"] as? UIImage
        
        requestImage((d!["image_thumb"] as? String)!) { (image) -> Void in
            cell.wonderImage = image
        }
        
    
        return cell
        
    }
    
    //start test
    
    
    
    func requestImage(url: String, success: (UIImage?) -> Void) {
        requestURL(url, success: { (data) -> Void in
            if let d = data {
                success(UIImage(data: d))
            }
        })
    }
    
    func requestURL(url: String, success: (NSData?) -> Void, error: ((NSError) -> Void)? = nil) {
        NSURLConnection.sendAsynchronousRequest(
            NSURLRequest(URL: NSURL (string: url)!),
            queue: NSOperationQueue.mainQueue(),
            completionHandler: { response, data, err in
                if let e = err {
                    error?(e)
                } else {
                    success(data)
                }
        })
    }
    
    //end test
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "detailview"){
            let cell = sender as! customcell
            let detailview = segue.destinationViewController as! DetailViewController
            detailview.preWonder = cell.Wonder
            detailview.preBuilt = cell.yearBuilt
            detailview.preLocation = cell.wonderLocation
            detailview.preRegion = cell.wonderRegion
            detailview.preImage = cell.wonderImage
            
        }
    }

}

