//
//  DetailViewController.swift
//  SwiftTest1
//
//  Created by Jason Clinger on 2/20/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var preWonder:String?
    var preBuilt:String?
    var preLocation:String?
    var preRegion:String?
    var preImage:UIImage?
    
    @IBOutlet weak var wonderTitle: UILabel!
    
    @IBOutlet weak var wonderLocation: UILabel!
    
    @IBOutlet weak var wonderRegion: UILabel!
    
    @IBOutlet weak var wonderImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    override func viewDidAppear(animated: Bool) {
        self.title = preWonder
        wonderTitle.text = preBuilt
        wonderLocation.text = preLocation
        wonderRegion.text = preRegion
        wonderImage.image = preImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
